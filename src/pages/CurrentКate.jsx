import React from "react";
import axios from "axios";

import s from "./CurrentКate.module.scss";

const CurrentКate = () => {

   const [inBankData, setInBankData] = React.useState();
   const [OnlineData, setOnlineData] = React.useState();

   const [overlay, setOverlay] = React.useState(false);
   const [blockInfo, setBlockInfo] = React.useState({});
   const [cahngeItem, setChangeItem] = React.useState(true);
   const [finalSum, setFinalSum] = React.useState(0);
   const [inputVal, setInputVal] = React.useState(0);


   const [getMoneyBlock, setGetMoneyBlock] = React.useState(false);
   const [card, setCard] = React.useState('');
   const [cardDate, setCardDate] = React.useState('');
   const [cardCv, setCardCv] = React.useState('');
   const [cardCvHash, setCardCvHash] = React.useState('');

   const [successBlock, setSuccessBlock] = React.useState(false);

   const courseInBank = 'https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5';
   const courseOnline = 'https://api.privatbank.ua/p24api/pubinfo?exchange&json&coursid=11';

   const getCourses = async () => {
      await axios.get(courseInBank).then(res => setInBankData(res.data));
      await axios.get(courseOnline).then(res => setOnlineData(res.data));
   }

   React.useEffect(() => {
      getCourses()
   }, []);

   const blockHnandler = (obj) => {
      setOverlay(true);
      setBlockInfo(obj);
   }

   const getMoney = (e) => {
      let val = +e.target.value;
      if (val < 0) return setInputVal(0);
      setInputVal(val);
      if (cahngeItem === true) {
         setFinalSum((val / blockInfo.buy).toFixed(2))
      } else {
         setFinalSum((val * blockInfo.buy).toFixed(2))
      }
   }

   const overlayHandler = () => {
      setOverlay(false);
      setFinalSum(0);
      setInputVal(0);
      setGetMoneyBlock(false);
      setSuccessBlock(false);
   }

   const changeHadler = () => {
      setChangeItem(!cahngeItem);
      setFinalSum(0);
      setInputVal(0);
   }

   const inputNumCard = (e) => {
      let val = e.target.value;
      let num = e.nativeEvent.data;
      let res = val.match(/\d{4}/g);
      if (isNaN(+num)) return;
      setCard(val);
      if (res === null) {
         setCard(val);
         return;
      }
      if (res.length === 4) {
         val = val.match(/\d{4}/g).join(' ');
         setCard(val);
      }
   }

   const inputCardDate = (e) => {
      let val = e.target.value;
      let num = e.nativeEvent.data;
      let res = val.match(/\d{2}/g);
      if (isNaN(+num)) return;
      setCardDate(val);
      if (res === null) {
         setCardDate(val);
         return;
      }
      if (res.length === 2) {
         val = val.match(/\d{2}/g).join('/');
         setCardDate(val);
      }
   }

   const inputCardCv = (e) => {
      let val = e.target.value;
      let num = e.nativeEvent.data;
      if (isNaN(+num) || val.length > 3) return;
      setCardCv(prev => prev + num);
      let resHash = val.replace(/\d+/, '*');
      setCardCvHash(resHash);
   }

   const btnNextStep = () => {
      if (+inputVal > 0) setGetMoneyBlock(true);
      setOverlay(false);
      setFinalSum(0);
      setInputVal(0);
   }

   const successBtn = () => {
      if (card.length === 19 && cardDate.length === 5 && cardCv.length === 3) {
         setGetMoneyBlock(false);
         setSuccessBlock(true);
         return;
      } else {
         alert('Заповніть всі поля');
      }
   }

   const finishBtn = () => {
      setSuccessBlock(false);
      setOverlay(false);

      setCardCv('');
      setCard('');
      setCardDate('');
      setCardCvHash('');

   }

   return (
      <div className={s.root}>
         {overlay && <>
            <div className={s.overlay} onClick={overlayHandler}></div>
            <div className={s.popap_block}>
               <div className={s.title_wrapper}>
                  <p className={s.title}>{blockInfo.base_ccy}</p>
                  <span onClick={changeHadler}>{cahngeItem ? '>' : '<'}</span>
                  <p className={s.title}>{blockInfo.ccy}</p>
               </div>
               <input onInput={getMoney} className={s.input} type="number" value={inputVal} />

               {cahngeItem ? <div className={s.description}>Іноземна валюта</div> : <div className={s.description}>Гривні</div>}
               {finalSum > 0 ? <div className={s.money}>{finalSum}</div> : <div className={s.money}>Сума не введена</div>}

               <button onClick={btnNextStep} className={s.BtnHandler}>Далі</button>
            </div>
         </>}

         {getMoneyBlock && <>
            <div className={s.overlay} onClick={overlayHandler}></div>
            <div className={s.card}>
               <div className={s.card_item}>
                  <label htmlFor="number_card">Номер карти</label>
                  <input onInput={inputNumCard} type="text" id="number_card" value={card} placeholder="0000 0000 0000 0000" />
               </div>
               <div className={s.card_item}>
                  <label htmlFor="data_card">Дата</label>
                  <input onInput={inputCardDate} value={cardDate} type="text" id="data_card" placeholder="10/25" />
               </div>
               <div className={s.card_item}>
                  <label htmlFor="cv_code">CV code</label>
                  <input onInput={inputCardCv} type="text" id="cv_code" placeholder="***" value={cardCvHash} />
               </div>
               <div className={s.card_item}>
                  <button onClick={successBtn}>Готово</button>
               </div>
            </div>
         </>
         }

         {successBlock && (
            <>
               <div className={s.overlay} onClick={overlayHandler}></div>
               <div className={s.success}>
                  <h3 >Операція пройшла успішно 🤙</h3>
                  <button onClick={finishBtn}>Завершити</button>
               </div>
            </>
         )}

         <h3>Купівля продаж у банку</h3>
         <div className={s.wrapper}>
            <div className={s.top_block}>
               {inBankData && inBankData.map(item => (
                  <div key={item.ccy}>
                     <h4>{item.ccy + ' > ' + item.base_ccy}</h4>
                     <p>Купівля {Number(item.buy).toFixed(3)}</p>
                     <p>Продаж {Number(item.sale).toFixed(3)}</p>
                  </div>
               ))}
            </div>

            <h3>Купівля продаж у онлайн</h3>
            <div className={s.bottom_block}>
               {OnlineData && OnlineData.map(item => (
                  <div onClick={() => blockHnandler(item)} key={item.ccy}>
                     <h4>{item.ccy + ' > ' + item.base_ccy}</h4>
                     <p>Купівля {Number(item.buy).toFixed(3)}</p>
                     <p>Продаж {Number(item.sale).toFixed(3)}</p>
                  </div>
               ))}
            </div>
         </div>
      </div >
   );
}

export default CurrentКate;