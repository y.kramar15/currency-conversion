import React from "react";
import s from "./Home.module.scss";
import ChangeCourse from "../components/ChangeCourse";
import History from "../components/History";

const Home = () => {
   return (
      <div className={s.home}>
         <ChangeCourse />
         <History />
      </div>
   );
}

export default Home;