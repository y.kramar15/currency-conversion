import React from 'react';
// styles =======================================
import s from './App.module.scss';
// Router-dom ===================================
import { Routes, Route } from "react-router-dom";
//Pages =========================================
import Home from './pages/Home';
//Component =====================================
import CurrentКate from './pages/CurrentКate';
import Header from './components/Header';


const App = () => {

  return (

    <div className={s.app}>
      <Header />
      <div className={s.container}>
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='current-value' element={<CurrentКate />} />
        </Routes>
      </div>

    </div>
  );
}

export default App;
