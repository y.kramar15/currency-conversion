import React from "react";

import Navigation from "./Navigation";

import s from "./Header.module.scss";

const Header = () => {
   const a = new Date();

   console.log(a);

   return (
      <div className={s.header}>
         <div className={s.container}>
            <Navigation />
         </div>
      </div>
   );
}

export default Header;