import React from "react";
import s from "./Header.module.scss"
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import close from "../../assets/images/close.png";

import data from "../../assets/data.json";

import { setInfo } from "../../redux/Slices/NavSlice";

const Navigation = () => {
   const info = useSelector(state => state.nav.info)
   const dispatch = useDispatch();
   const [val, setVal] = React.useState('');
   const [active, setActive] = React.useState(false);

   React.useEffect(() => {
      dispatch(setInfo(data));
   }, []);

   const searchCountry = (e) => {
      let val = e.target.value.trim().toLowerCase();
      val === '' ? setActive(false) : setActive(true);

      setVal(val);
      const searchInfo = data.filter(item => item.txt.trim().toLowerCase().search(val) >= 0);
      dispatch(setInfo(searchInfo));
   }

   const [nav, setNav] = React.useState(0);
   const navigation = [
      { name: 'конвертор', path: '/' },
      { name: 'поточні курси', path: 'current-value' }
   ];

   const closeBtn = (e) => {
      setVal('');
      setActive(false);
      dispatch(setInfo(data));
   }

   return (
      <div className={s.root}>
         <ul className={s.nav}>
            {navigation.map((item, index) => (
               <li key={index}>
                  <Link
                     onClick={() => setNav(index)}
                     className={index === nav ? s.active : ''}
                     to={item.path}
                  >
                     {item.name}
                  </Link>
               </li>
            ))}
         </ul>
         <div className={s.search_block}>
            <div className={s.search_wrapper}>
               <span onClick={closeBtn}><img src={close} alt="" /></span>
               <input onInput={searchCountry} type="text" value={val} placeholder="Пошук" />
            </div>
            {
               // active && info.length !== 0 && <div className={s.active_block}>
               //    <ul >
               //       Валюти по вашому запиту
               //       <hr />
               //       {info.map(item => <li onClick={(e) => console.log(e)} key={item.r030}>{item.txt}</li>)}
               //    </ul>
               // </div>
            }
         </div>

      </div >
   );
};


export default Navigation;