import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { setInputValue, setChangeCountry, setResult } from "../../redux/Slices/ChangeSlice";
import { setHistory } from "../../redux/Slices/HistorySlice";
import dataJson from "../../assets/data.json";
import s from "./ChangeCourse.module.scss";



const ChangeCourse = () => {
   const dispatch = useDispatch();
   // Selectors data ==========================================================
   // const data = useSelector(data => data.data.data);
   // Selectors change ==========================================================
   const inputValue = useSelector(data => data.change.inputValue);
   const changeCountry = useSelector(data => data.change.changeCountry);
   const result = useSelector(data => data.change.result);
   // Selectors history =======================================================
   const history = useSelector(data => data.history.history);
   //state =================================================================
   const data = useSelector(data => data.nav.info);


   const optionData = (e) => {
      const arr = e.target.childNodes;
      const index = e.target.selectedIndex;
      let checPoint = true;

      if (history === undefined) {
         dispatch(setHistory([data[index]]))
         return;
      }

      for (let i = 0; i < history.length; i++) {
         if (+history[i].r030 === +arr[index].id) checPoint = false;
      }
      if (checPoint === false) return;
      dispatch(setHistory([...history, data[index]]));
   }

   const getKey = (e) => {
      const num = +e.target.value
      const roundNum = num.toFixed(3);
      dispatch(setChangeCountry(roundNum));
      optionData(e);
   }

   const onInputNumber = (e) => {
      (+e.target.value < 1) ? dispatch(setInputValue(1)) : dispatch(setInputValue(+e.target.value));
   };

   const handlerBtn = () => {
      dispatch(setResult((changeCountry * inputValue).toFixed(3)));
   };

   return (
      <>
         <div className={s.root}>
            <div className={s.block_left}>
               <label className={s.label} htmlFor="money">Виберіть валюту: </label>

               <select size={data.length < dataJson.length ? data.length : 0} open onChange={getKey} name="money" id="money">
                  {data.length === 0 ? <option>такої країни немає</option> : data.map((item) => {
                     return (
                        <option key={item.r030} value={item.rate} id={item.r030}>{item.txt}</option>
                     );
                  })}
               </select>
            </div>
            <div>
               <div>
                  <label htmlFor="num">
                     Введіть кількість:
                  </label>
                  <input
                     className={s.amount}
                     name="num"
                     id="num"
                     type="number"
                     value={inputValue}
                     onInput={onInputNumber}
                  />
               </div>
               <button onClick={handlerBtn} >Рахувати</button>
            </div>
            <div>
               {result}
               &#8372;
            </div>
         </div>

      </>
   );
}

export default ChangeCourse;