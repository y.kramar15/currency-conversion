import React from "react";
import s from "./History.module.scss";
import { useSelector, useDispatch } from "react-redux";
import { removeHistory } from "../../redux/Slices/HistorySlice";

const History = () => {
   const dispatch = useDispatch();
   const history = useSelector(state => state.history.history);

   const remove = (event) => {
      if (event.target.tagName.toLowerCase() !== 'div') return;
      dispatch(removeHistory(+event.target.getAttribute('index')));
   }

   const [count, setCount] = React.useState(1);

   const calculate = (event) => {
      let num = +event.target.value;
      (num < 1) ? setCount(1) : setCount(num);
   };




   return (
      <div className={s.root}>
         <h3>переглянуті курси:</h3>
         <table>
            <thead>
               <tr>
                  <th>держава</th>
                  <th>одиниця</th>
                  <th>в гривні</th>
                  <th>назва валюти</th>
                  <th>видалити</th>
               </tr>
            </thead>
            <tbody >

               {history && history.map((item, index) => (
                  <tr key={item.r030}>
                     <td index={index}>{item.txt}</td>
                     <td><input index={index} type="number" onInput={calculate} value={count} /></td>
                     <td index={index}>{(count * item.rate).toFixed(2)}&#8372;</td>
                     <td index={index}><span>{item.cc}</span></td>
                     <td onClick={remove} index={index}><div>X</div></td>
                  </tr>
               ))}

            </tbody>
         </table>
      </div>
   );
}

export default History;