import { configureStore } from "@reduxjs/toolkit";
import data from "./Slices/DataSlice";
import change from "./Slices/ChangeSlice";
import history from "./Slices/HistorySlice";
import nav from "./Slices/NavSlice";


const store = configureStore({
   reducer: {
      data,
      change,
      history,
      nav
   }
});

export default store;
