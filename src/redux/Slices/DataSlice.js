import { createSlice } from "@reduxjs/toolkit";
import data from "../../assets/data.json"

const DataSlice = createSlice({
   name: 'data',
   initialState: {
      // firstValue: data[0].rate.toFixed(3),
      data
   },
   reducers: {},
});

export default DataSlice.reducer;