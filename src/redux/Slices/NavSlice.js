import { createSlice } from "@reduxjs/toolkit";

const NavSlice = createSlice({
   name: 'nav',
   initialState: {
      info: []
   },
   reducers: {
      setInfo: (state, { payload }) => {
         state.info = payload;
      }
   },

});

export const { setInfo } = NavSlice.actions;

export default NavSlice.reducer;