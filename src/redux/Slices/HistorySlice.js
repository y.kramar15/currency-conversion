import { createSlice } from "@reduxjs/toolkit";

const HistorySlice = createSlice({
   name: 'history',
   initialState: {},
   reducers: {
      setHistory: (state, { payload }) => {
         state.history = payload;
      },
      removeHistory: (state, { payload }) => {
         const newState = state.history.filter((_, index) => payload !== index);
         state.history = newState;
      }
   },
});

export const { setHistory, removeHistory } = HistorySlice.actions;

export default HistorySlice.reducer;