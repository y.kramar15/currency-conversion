import { createSlice } from "@reduxjs/toolkit";
import data from "../../assets/data.json"

const ChangeSlice = createSlice({
   name: 'change',
   initialState: {
      inputValue: 1,
      changeCountry: data[0].rate.toFixed(3),
      result: 0,
      history: [],
   },
   reducers: {
      setInputValue: (state, { payload }) => {
         state.inputValue = payload;
      },
      setChangeCountry: (state, { payload }) => {
         state.changeCountry = payload;
      },
      setResult: (state, { payload }) => {
         state.result = payload;
      }
   }
});

export const { setInputValue, setChangeCountry, setResult } = ChangeSlice.actions;

export default ChangeSlice.reducer;